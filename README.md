mongodb-samples
=================
# Table of contents

1. [Introduction](#1-introduction)
2. [Detail](#2-Samples)
  1. [single instance](#21-single)
  2. [cluster](#22-cluster)
  3. [utilities](#32-utilities)

# 1. Introduction
samples for rabbitmq

## 2.1. single

To install: go to ./single
```bash
docker-compose up -d --build
```

## 2.2. cluster

To install: go to ./cluster
```bash
docker-compose up -d --build
```

## 3.2. cluster

Memento

rabbitmqctl Help:
```bash
docker-compose exec rabbitmq1 sh -c "/usr/sbin/rabbitmqctl -h"
```

list queues:
```bash
docker-compose exec rabbitmq1 sh -c "/usr/sbin/rabbitmqctl list_queues"
```

See generated rabbitmq config file:
```bash
docker-compose exec rabbitmq1 sh -c "cat /var/lib/rabbitmq/config/generated/rabbitmq.config"
```

vhosts:

Add a host
```bash
docker-compose exec rabbitmq1 sh -c "/usr/sbin/rabbitmqctl add_vhost vh1"
```

Delete a host
```bash
docker-compose exec rabbitmq1 sh -c "/usr/sbin/rabbitmqctl delete_vhost vh1"
```

Delete a host
```bash
curl -u guest:guest -X PUT http://localhost:15672/api/vhosts/vh1
```

Delete a host
```bash
curl -u guest:guest -X DELETE http://localhost:15672/api/vhosts/vh1
```

Delete a host
```bash
docker exec -it rabbitmq /bin/sh -c "/usr/sbin/rabbitmqctl restart_vhost -p /vh1"

Utilities:

Add a queue on / virtual host
```bash
curl -i -u guest:guest -H "content-type:application/json" -XPUT -d"{}"  http://localhost:15672/api/queues/%2F/queueDef


```